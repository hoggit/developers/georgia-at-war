write_state = function()
    log("Writing State...")
    local stateFile = lfs.writedir()..[[Scripts\GAW\state.json]]
    local fp = io.open(stateFile, 'w')
    fp:write(json:encode(game_state))
    fp:close()
    log("Done writing state.")
end

mist.scheduleFunction(write_state, {}, timer.getTime() + 524, 580)

-- update list of active CTLD AA sites in the global game state
function enumerateCTLD()
    local hawkCTLDstate = {}
    local NASAMSCTLDstate = {}
    local patriotCTLDstate = {}
    --log("Enumerating CTLD")
    for _groupname, _groupdetails in pairs(ctld.completeAASystems) do
        local _siteDetail = {}
        local _lnPosArray = {}
        local _launcherPart
        for _,v in pairs(_groupdetails) do
            _launcherPart = _launcherPart or ctld.getLauncherUnitFromAATemplate(v['system'])
            if v['unit'] == _launcherPart then
                table.insert(_lnPosArray, v['point'])
            end
            _siteDetail[v['unit']] = {
                x = v['point'].x,
                z = v['point'].z,
                pos = v['point'],
                hdg = v['hdg']
            }
        end
        local _centroid = ctld.getCentroid(_lnPosArray)
        _siteDetail[_launcherPart]['x'] = _centroid.x
        _siteDetail[_launcherPart]['z'] = _centroid.z
        _siteDetail[_launcherPart]['pos'] = _centroid

        if string.match(_groupname, "Hawk") then
            hawkCTLDstate[_groupname] = _siteDetail

        elseif string.match(_groupname, "NASAMS") then
            NASAMSCTLDstate[_groupname] = _siteDetail

        elseif string.match(_groupname, "Patriot") then
            patriotCTLDstate[_groupname] = _siteDetail

        end
    end
    game_state["Hawks"] = hawkCTLDstate
    game_state["NASAMS"] = NASAMSCTLDstate
    game_state["Patriots"] = patriotCTLDstate
    log("Done Enumerating CTLD")
end

ctld.addCallback(function(_args)
    if _args.action and _args.action == "unpack" then
        local name
        local unit = _args.spawnedGroup:getUnit(1)
        local groupname = _args.spawnedGroup:getName()
        local groupname_mappings = {
            ["Avenger"] = "avenger",
            ["M 818"] = "ammo",
            ["Gepard"] = "gepard",
            ["MLRS"] = "mlrs",
            ["Hummer"] = "jtac",
            ["Abrams"] = "abrams",
            ["Chaparral"] = "chaparral",
            ["Vulcan"] = "vulcan",
            ["M-109"] = "M-109",
            ["Roland"] = "roland",
            ["Tanker"] = "tanker",
            ["C-RAM"] = "c-ram",
        }
        log("Unpacked crate: " .. groupname)
        for key, value in pairs(groupname_mappings) do
            if string.match(groupname, key) then
                name = value
                break
            end
        end

        if name then
            table.insert(game_state["CTLD_ASSETS"], {
                name = name,
                pos = unit:getPosition().p,
                hdg = mist.getHeading(unit, true)
            })
        end

        if string.match(groupname, "Hawk") or
           string.match(groupname, "NASAMS") or 
           string.match(groupname, "Patriot") then
            enumerateCTLD()
        end

        write_state()
    end
end)
